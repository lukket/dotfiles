# dotfiles

## .gitconfig

```
[include]
	path = src/dotfiles/gitconfig
```

## Tmux

### Plugin Manager

```
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
tmux source ~/.tmux.conf
```
Press `prefix` + `I` (capital i, as in Install) to fetch the plugin.

## Vim

### Plugin Manager

```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
## SSH

### Store passphrase in the Keychain (MacOS)

```
ssh-add --apple-use-keychain ~/.ssh/[your-private-key]
```

## Docker

### Zsh Completion

```
etc=/Applications/Docker.app/Contents/Resources/etc
ln -s $etc/docker.zsh-completion /usr/local/share/zsh/site-functions/_docker
ln -s $etc/docker-compose.zsh-completion /usr/local/share/zsh/site-functions/_docker-compose
```

## Powerlevel10k theme for Zsh

```
git clone --depth=1 https://gitee.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
brew install tmux-mem-cpu-load
```

## Nx

### Zsh Completion

```
brew install jq
git clone git@github.com:jscutlery/nx-completion.git ~/.nx-completion
```
